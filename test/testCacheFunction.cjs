const cache_Function_Obj = require('../cacheFunction.cjs');

try {
    const cb = (args) => {
        args += 1;
        return args;
    }
    let result = cache_Function_Obj(cb);
    // result();
    console.log(result(1));
    console.log(result(2));
    console.log(result(2));
    console.log(result(3));
}
catch(error){
    console.log(error);
}