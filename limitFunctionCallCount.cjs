const limitFunctionCallCount = (cb, n) => {

        if(typeof cb !== 'function' || typeof n !== "number"){
            throw new Error('Wrong arguments passed');
        }
        let count = n;

        return function cbFunction(...args) {

            if (count > 0) {
                --count;
                return cb(...args);
            }
            else {
                return null;
            }
        }

};

module.exports = limitFunctionCallCount;