const cache_Function = (cb) =>
{
    if(typeof cb !== 'function')
    {
        throw new Error('Callback function invalid');
    }
    let cache_Obj = {};
    return function (...argument)
    {
        let key = JSON.stringify(argument);
        if(!(key in cache_Obj))
        {
            let result = cb(...argument);
            cache_Obj[key] = result;
            return result;
        }
        else
        {
            return cache_Obj[key];
        }
    }
};
module.exports = cache_Function;